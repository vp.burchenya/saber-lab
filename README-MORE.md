### CI/DI
- Just for example please see `.gitlab-ci.yaml`


### Kubernetes native
- Download and unpack: [flink-1.11.3-bin-scala_2.12](https://www.apache.org/dyn/closer.lua/flink/flink-1.11.3/flink-1.11.3-bin-scala_2.12.tgz)
- ```bash
  docker build -t vpburchenya/saber-sesscount -f job.dockerfile .
  docker push vpburchenya/saber-sesscount
  kk config current-context
  kk create ns saber
  ./bin/flink run-application --target kubernetes-application \
  -Dkubernetes.rest-service.exposed.type=NodePort \
  -Dkubernetes.namespace=saber \
  -Dkubernetes.cluster-id=flink-saber-sesscount \
  -Dkubernetes.container.image=vpburchenya/saber-sesscount \
  local:///opt/flink/jobs/saber-sesscount-job.jar \
  -Dssl.truststore.location="/opt/flink/keystore.jks \
  -Dstart.mode=latest
  ```

- [The Strimzi Kafka Operator](KafkaDeploy.yaml)
- [Flink app's job ingress](Ingress.yaml)
