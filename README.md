### <font color=red>Notice:</font> All below is true for Linux Debian bullseye release

### Install Git and clone the repo
  ```bash
  sudo -i apt install git
  git clone https://gitlab.com/vp.burchenya/saber-lab.git
  cd saber-lab
  ```

### Install Docker and DockerCompose
1. #####Install Docker Engine
   ```bash
    sudo -i apt remove docker docker-engine docker.io containerd runc
    sudo -i apt install apt-transport-https ca-certificates wget software-properties-common
    wget -O- https://download.docker.com/linux/debian/gpg | sudo -i apt-key --keyring /etc/apt/trusted.gpg.d/docker-ce.gpg add -
    echo 'deb [arch=amd64] https://download.docker.com/linux/debian buster stable' | sudo -i tee /etc/apt/sources.list.d/docker-ce.list
    sudo -i apt update
    sudo -i apt install docker-ce docker-ce-cli containerd.io
    sudo -i gpasswd -a $USER docker
    # !!! RELOGIN from X11 (or reboot) !!!
    sudo -i docker run --rm hello-world  # <-- test docker installation 
    ```

2. #####Install DockerCompose
   - From PyPi
     ```bash
     python3 -m venv .venv && .venv/bin/activate
     pip install -U pip docker-compose
     ```

   - Or from dist repo
     ```bash
     sudo -i apt install docker-compose
     ```

### Zeppelin SQL batch Job
- Reads local JSON samples and calculate session time (output is filtered by a concrete session UUID - `SQL WHERE` clause)
  Zeppelin Dockerfile is provided: [zp.dockerfile](zp.dockerfile) 

- #####Run Zeppelin
    ```bash
    docker-compose up --build zp
    ```

- Execute The Zeppelins notebook: [`SaberSessCountLab`](http://localhost:8040/#/notebook/2FYY19JSS)

- See that the delta diff for session uuid `d69a9f3a-c0d9-11e8-89d3-309c2303d212` is equals to `2471225 ms`.

  ![Zeppelin Notebook](img/ZeppelinUI.png "Zeppelin Notebook")

### Jupyter Pandas Batch IPython notebook
- Reads local JSON samples and calculate session time (output is filtered by a concrete session UUID - `Pandas DataFrame filter`)

- ##### [Minimalistic Notebook at CoLab](https://colab.research.google.com/drive/19XNJECtovhLR0_gthstGwmQUeAKNuVBQ?usp=sharing)

  ![CoLab Notebook](img/CoLabUI.png "CoLab Notebook")

- ##### You can execute a local notebook (if you want) with more numbers of experiments (LeftJoin+GroupBy batch)
  - ```bash
    [ -n "${_OLD_VIRTUAL_PATH:-}" ] || python3 -m venv .venv ; . .venv/bin/activate
    pip install -U pip jupyterlab
    jupyter lab --port=8888
    ```

  - See Notebook: [UserSessions.ipynb](http://localhost:8888/lab/tree/UserSessions.ipynb)

    ![Jupyter Notebook](img/JupyterUI.png "Jupyter Notebook")

### Apache Flink Streaming Job
- Reads kafka topic `com.saber3d.sessions` for a JSON events, calculates a session time diff
  and sends results to Kafka topic `com.saber3d.collect`
  Flink Job Dockerfile is provided: [job.dockerfile](job.dockerfile)

- #####Run Flink JobManager and TaskManager
    ```bash
    docker-compose up --build flink-master flink-worker
    ```
- #####See for job info [Job WebUI](http://localhost:8081/#/job/0123456789aaaabbccddeeffffffffff/overview)

  ![Flink Job UI](img/FlinkJobUI.png "Job WebUI")

- #####Install KafkaCat
  `sudo -i apt install kafkacat`

- #####Open two additional terminal windows (you can also use tmux multiplexer) and change dir to this repository dir
  - In first terminal execute, the script consume messages from `com.saber3d.collect` topic
    `./demo-message-comsume.sh`

  - In second terminal execute, the script produce two session events to `com.saber3d.collect` topic with a second delay
  
    `./demo-message-produce.sh login && sleep 1 && ./demo-message-produce.sh logout`

  - See that you see message at first terminal with calculated session delta time diff

    ![Consumer screenshot](img/consume-screenshot.png "Consumer")

- #####You can also start Flink Job from IDE or by Gradle
  Use commands: `./demo-message-produce.sh login` and `./demo-message-produce.sh logout` to send events to the Kafka
  topic and see that messages are processed by Flink Job.

  ![Idea Flink Job Log](img/IdeaFlinkJobLog.png "Consumer")
