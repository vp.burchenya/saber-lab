#!/bin/bash
set -eu -o pipefail

BASE_DIR=$(realpath $(dirname "$0"))


for t in sessions collect; do
    kafkacat -L -b kafka.oz.net.ru:30094 \
      -X sasl.username=saber3d \
      -X sasl.password=$(cat "$BASE_DIR/kafka.password") \
      -X sasl.mechanism=SCRAM-SHA-512 \
      -X security.protocol=SASL_SSL \
      -X ssl.ca.location="$BASE_DIR/kafka.pem" \
      -X enable.ssl.certificate.verification=true \
      -t "com.saber3d.$t" ;
done

