FROM apache/zeppelin:0.9.0


WORKDIR /zeppelin/

RUN wget -O- \
  "https://downloads.apache.org/flink/flink-1.11.3/flink-1.11.3-bin-scala_2.11.tgz" \
  | tar -xvz

# Deps:
# ---
# https://repo1.maven.org/maven2/org/apache/flink/flink-sql-connector-kafka_2.11/1.11.3/flink-sql-connector-kafka_2.11-1.11.3.jar
RUN \
  wget -P ./flink-1.11.3/lib/ \
  "https://repo.maven.apache.org/maven2/org/apache/flink/flink-sql-connector-kafka_2.11/1.11.3/flink-sql-connector-kafka_2.11-1.11.3.jar"
# ===

COPY kafka.pem ./
