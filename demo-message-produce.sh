#!/bin/bash
set -eux -o pipefail


# TODO: substr from link
(test "$*" == 'login' || test "$*" == "logout") || (echo "Usage: $0 <login|logout>" ; exit -1)

BASE_DIR=$(realpath $(dirname "$0"))


cat DemoKafkaSessionEvent.json \
  | env TYPE="$*" TS=$(date +%s)000 SESSION_UUID=4869efdb-ac0d-4919-9a49-4d38dbe93079 envsubst \
  | tee /dev/stderr | tr -d "\n" | tr -d ' ' \
  | kafkacat -P -b kafka.oz.net.ru:30094 \
    -X sasl.username=saber3d \
    -X sasl.password=$(cat "$BASE_DIR/kafka.password") \
    -X sasl.mechanism=SCRAM-SHA-512 \
    -X security.protocol=SASL_SSL \
    -X ssl.ca.location="$BASE_DIR/kafka.pem" \
    -X enable.ssl.certificate.verification=true \
    -t com.saber3d.sessions

