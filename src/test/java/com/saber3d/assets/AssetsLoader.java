package com.saber3d.assets ;

import java.io.InputStream ;


public class AssetsLoader {
    private static final Class<?> BASE_ASSET_LOADER = AssetsLoader.class ;

    public InputStream loadAsset(String ... name) {
        return BASE_ASSET_LOADER.getResourceAsStream(String.join("/", name)) ;
    }

    public InputStream loadAsset(String name) {
        return getClass().getResourceAsStream(name) ;
    }
}
