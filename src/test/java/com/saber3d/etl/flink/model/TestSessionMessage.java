package com.saber3d.etl.flink.model ;

import com.saber3d.etl.flink.ser.JsonKeyValueDeserializationSchema ;
import com.saber3d.etl.flink.ser.SessionMessageToDeserializationSchema ;

import jakarta.validation.ConstraintViolation ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode ;
import org.apache.kafka.clients.consumer.ConsumerRecord ;

import org.junit.Before ;
import org.junit.BeforeClass ;
import org.junit.Test ;

import java.io.IOException ;
import java.io.InputStream ;
import java.util.Set;
import java.util.UUID ;
import java.time.Instant ;
import java.time.ZoneId ;
import java.time.ZonedDateTime ;

import com.saber3d.assets.AssetsLoader ;
import static org.junit.Assert.* ;


public class TestSessionMessage {
    private static AssetsLoader ASSETS ;
    private ObjectMapper mapper ;

    @BeforeClass
    public static void setupAssets() {
        ASSETS = new AssetsLoader() ;
    }

    @Before
    public void setupMapper() { mapper = JsonKeyValueDeserializationSchema.createDefaultMapper() ;}

    @Test
    public void testBodyLoad() throws IOException {
        InputStream is = ASSETS.loadAsset("KafkaSessionEvent.json") ;
        SessionMessageTo msg = mapper.readValue(is, SessionMessageTo.class) ;
        _testBodyLoad(msg) ;
        Set<ConstraintViolation<SessionMessageTo>> errors = ValidatorLazySingleton.getInstance().validate(msg) ;
        errors.forEach(e -> System.err.printf("Validation error [%s]: %s\n", e.getPropertyPath(), e.getMessage())) ;
        assertEquals(0, errors.size()) ;
    }

    @Test
    public void testKafkaMessageKvLoad() throws IOException {
        InputStream is = ASSETS.loadAsset("KafkaSessionEvent.json") ;

        ConsumerRecord<byte[], byte[]> record = new ConsumerRecord<>(
            "demo", 0, 0,
            UUID.randomUUID().toString().getBytes(), is.readAllBytes()
        ) ;

        JsonKeyValueDeserializationSchema deserializer = new JsonKeyValueDeserializationSchema(true) ;
        ObjectNode root = deserializer.deserialize(record) ;
        assertFalse(root.get("error").asBoolean()) ;

        _testBodyLoad(mapper.treeToValue(root.get("ev"), SessionMessageTo.class)) ;
    }

    @Test
    public void testKafkaMessageEntityLoad() throws IOException {
        InputStream is = ASSETS.loadAsset("KafkaSessionEvent.json") ;

        ConsumerRecord<byte[], byte[]> record = new ConsumerRecord<>(
            "demo", 0, 0,
            UUID.randomUUID().toString().getBytes(), is.readAllBytes()
        ) ;

        SessionMessageToDeserializationSchema deserializer = new SessionMessageToDeserializationSchema() ;
        SessionMessageTo to = deserializer.deserialize(record) ;

        assertNotNull(to) ;
        _testBodyLoad(to) ;
    }

    private void _testBodyLoad(SessionMessageTo msg) {
        assertEquals(UUID.fromString("5f1e0664-993a-4992-8a2f-5dd2c5ee82d0"), msg.getUserUUID()) ;
        assertEquals(UUID.fromString("2eedfd31-7031-407b-8eee-5e5a0aeba745"), msg.getSessionUUID()) ;
        assertEquals(EventType.LOGIN, msg.getType()) ;
        assertNull(msg.getPrevSessionUUID()) ;
        assertEquals(
            ZonedDateTime.of(2018, 10, 20, 1, 46, 39, 999000000, ZoneId.of("UTC")).toInstant(),
            msg.getDateTime().toInstant()
        ) ;
        assertEquals("RU", msg.getCountry()) ;
        assertTrue(msg instanceof SessionLoginMessageTo) ;
    }

    @Test
    public void testTs() {
        final long ts = 1539999999999L ;
        assertEquals(
            Instant.ofEpochMilli(ts),
            ZonedDateTime.of(2018, 10, 20, 1, 46, 39, 999000000, ZoneId.of("UTC")).toInstant()
        ) ;
    }
}
