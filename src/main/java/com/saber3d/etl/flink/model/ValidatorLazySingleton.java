package com.saber3d.etl.flink.model ;

import jakarta.validation.Validation ;
import jakarta.validation.Validator ;


public class ValidatorLazySingleton {
    protected final Validator validator ;

    public ValidatorLazySingleton() {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator() ;
    }

    private static class OnDemandHolder {
        public static final ValidatorLazySingleton INSTANCE = new ValidatorLazySingleton() ;
    }

    public static Validator getInstance() {
        return OnDemandHolder.INSTANCE.validator ;
    }
}
