package com.saber3d.etl.flink.model ;


import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonProperty;

public enum EventType {
    @JsonProperty("login")
    LOGIN,
    @JsonProperty("logout")
    LOGOUT
}
