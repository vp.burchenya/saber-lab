package com.saber3d.etl.flink.model ;

import jakarta.validation.constraints.NotNull ;
import org.jetbrains.annotations.Nullable ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.* ;

import java.util.Date ;
import java.util.UUID ;


@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "Type",
    visible = true,
    defaultImpl = SessionMessageTo.class
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = SessionLoginMessageTo.class, name = "login"),
    @JsonSubTypes.Type(value = SessionLogoutMessageTo.class, name = "logout"),
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionMessageTo {
    @NotNull
    @JsonProperty("UserUid")
    private UUID userUUID ;
    @NotNull
    @JsonProperty("UserSessionUid")
    private UUID sessionUUID ;
    @NotNull
    @JsonProperty("Type")
    private EventType type ;
    @Nullable
    @JsonProperty("PrevSession")
    private UUID prevSessionUUID ;
    @NotNull
    @JsonProperty("Time")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT, timezone = "UTC")
    private Date dateTime ;
    @Nullable
    @JsonProperty("Country")
    private String country ;

    public UUID getUserUUID() { return userUUID ;}
    public UUID getSessionUUID() { return sessionUUID ;}
    public EventType getType() { return type ;}
    public @Nullable UUID getPrevSessionUUID() { return prevSessionUUID ;}
    public Date getDateTime() { return dateTime ;}
    public @Nullable String getCountry() { return country ;}

    @Override
    public String toString() {
        return "SessionMessageTo{" +
            "sessionUUID=" + sessionUUID +
            ", type=" + type +
            ", dateTime=" + dateTime +
        '}';
    }
}
