package com.saber3d.etl.flink.job ;

import com.saber3d.etl.flink.kafka.BaseKafkaProducer ;
import com.saber3d.etl.flink.kafka.SessionMessageToKafkaConsumer ;
import com.saber3d.etl.flink.model.SessionMessageTo ;

import org.apache.flink.api.java.utils.ParameterTool ;

import org.apache.flink.api.java.tuple.Tuple2 ;

import org.apache.flink.api.common.restartstrategy.RestartStrategies ;
import org.apache.flink.streaming.api.CheckpointingMode ;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator ;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment ;

import org.apache.flink.streaming.api.TimeCharacteristic ;
import org.apache.flink.api.common.eventtime.WatermarkStrategy ;
import org.apache.flink.streaming.api.windowing.assigners.GlobalWindows ;
import org.apache.flink.streaming.api.functions.ProcessFunction ;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction ;

import org.apache.flink.streaming.api.functions.sink.SinkFunction ;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer ;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema ;
import org.apache.kafka.clients.producer.ProducerRecord ;

import org.apache.flink.util.Collector ;
import org.apache.flink.util.OutputTag ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;

import java.nio.charset.StandardCharsets ;
import java.util.Optional;
import java.util.UUID ;
import java.time.Instant ;


public class SessionCountJob {
    private static final Logger LOG = LoggerFactory.getLogger(SessionCountJob.class) ;

    @SuppressWarnings("RedundantThrows")
    public static void main(String[] args) throws Exception {
        final String inputTopic = "com.saber3d.sessions" ;
        final String outputTopic = "com.saber3d.collect" ;

        LOG.info("start job input={} output={}", inputTopic, outputTopic) ;
        final OutputTag<SessionMessageTo> errorTag = new OutputTag<>("error") {} ;
        final StreamExecutionEnvironment env = getEnv() ;
        final FlinkKafkaConsumer<SessionMessageTo> consumer = new SessionMessageToKafkaConsumer(inputTopic) ;

        String startMode = null ;  // StartupMode is private
        switch (ParameterTool.fromSystemProperties().get(
            "start.mode",
            ParameterTool.fromArgs(args).get("start-mode", "")
        )) {
            case "latest"   : break ;
            case "earliest" : consumer.setStartFromEarliest() ; startMode = "earliest" ; break ;
            case "group"    : consumer.setStartFromGroupOffsets() ; startMode = "group offsets" ; break ;
            default         : consumer.setStartFromLatest() ; startMode = "latest" ;
        }
        LOG.info("start consume from {}:{} to {}", inputTopic, startMode, outputTopic) ;

        SingleOutputStreamOperator<SessionMessageTo> ds =
        env
        .addSource(consumer)
        .setParallelism(1)  // Be careful with kafka partitions, parallelism and watermarks assigner
        .assignTimestampsAndWatermarks(WatermarkStrategy
            //.forGenerator(ctx -> new MyWatermarkGenerator())
            .<SessionMessageTo>forMonotonousTimestamps()
            .withTimestampAssigner((to, ts) -> to.getDateTime().toInstant().toEpochMilli())
        )
        .process(new ProcessFunction<>() {
            @Override
            public void processElement(SessionMessageTo to, Context ctx, Collector<SessionMessageTo> out) throws Exception {
            LOG.debug(
                "income user.uuid={} session.uuid={} time='{}' ts='{}' watermark.ts='{}'",
                to.getUserUUID(), to.getSessionUUID(), to.getDateTime().toInstant(),
                Instant.ofEpochMilli(ctx.timestamp()),
                Instant.ofEpochMilli(ctx.timerService().currentWatermark())
            ) ;
            out.collect(to) ;
            }
        })
        //.name("events")
        ;

        SingleOutputStreamOperator<Tuple2<UUID, Long>> res = ds
        .keyBy(SessionMessageTo::getSessionUUID)
        .window(GlobalWindows.create())
        .trigger(SessionMessageToTrigger.create())
        .process(new ProcessWindowFunction<>() {
            @Override
            public void process(UUID session, Context ctx, Iterable<SessionMessageTo> events, Collector<Tuple2<UUID, Long>> out) throws Exception {
                LOG.debug(
                    "windows processing.ts={} watermark.ts={} ",
                    Instant.ofEpochMilli(ctx.currentProcessingTime()),
                    Instant.ofEpochMilli(ctx.currentWatermark())
                ) ;
                events.forEach(System.out::println) ;
                // SHIT_CODE: We should use the Streams API reduce, but below is more readable and simple
                // Success for unordered d69a9f3a-c0d9-11e8-89d3-309c2303d212
                Tuple2<Long, Long> res = new Tuple2<>(null, null) ;
                for (SessionMessageTo to : events) {
                    long ts = to.getDateTime().toInstant().toEpochMilli() ;
                    long start = Math.min(Optional.ofNullable(res.f0).orElse(ts), ts) ;
                    long end = Math.max(Optional.ofNullable(res.f1).orElse(ts), ts) ;
                    res.f0 = start ;
                    res.f1 = end ;
                }
                Tuple2<UUID, Long> container = new Tuple2<>(session, res.f1 - res.f0) ;
                out.collect(container) ;
            }
        })
        ;

        res
        .addSink(new SinkFunction<>() {
            @Override
            public void invoke(Tuple2<UUID, Long> res, Context ctx) throws Exception {
                LOG.info("collect session={} ms={}", res.f0, res.f1) ;
            }
        })
        .name("print")
        ;

        res.addSink(new BaseKafkaProducer<>(
            outputTopic,  // default topic
            (KafkaSerializationSchema<Tuple2<UUID, Long>>) (t, ts) -> new ProducerRecord<>(
                outputTopic,
                t.f0.toString().getBytes(StandardCharsets.UTF_8),
                String.format("%s = %s", t.f0, t.f1).getBytes(StandardCharsets.UTF_8)
            )
        ))
        .name("kafka")
        ;

        ds.getSideOutput(errorTag).addSink(new SinkFunction<>() {
            @Override
            public void invoke(SessionMessageTo to, Context ctx) throws Exception {
                System.err.println(to) ;
            }
        }) ;

        LOG.debug(env.getExecutionPlan()) ;
        env.disableOperatorChaining().execute(SessionCountJob.class.getCanonicalName()) ;
    }

    private static StreamExecutionEnvironment getEnv() {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment() ;
        env.setRestartStrategy(RestartStrategies.noRestart()) ;
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime) ;
        env.enableCheckpointing(60*1000 , CheckpointingMode.EXACTLY_ONCE) ;  // Disable consumer autocommit
        env.disableOperatorChaining() ;
        env.getConfig().setAutoWatermarkInterval(1000) ;
        return env ;
    }
}
