package com.saber3d.etl.flink.job ;

import com.saber3d.etl.flink.model.EventType ;
import com.saber3d.etl.flink.model.SessionMessageTo ;
import org.apache.flink.streaming.api.windowing.triggers.Trigger ;
import org.apache.flink.streaming.api.windowing.triggers.TriggerResult ;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;


public class SessionMessageToTrigger extends Trigger<SessionMessageTo, GlobalWindow> {
	private static final long serialVersionUID = 1L ;
	private static final Logger LOG = LoggerFactory.getLogger(SessionMessageToTrigger.class) ;

	@Override
	public TriggerResult onElement(SessionMessageTo to, long timestamp, GlobalWindow window, TriggerContext ctx) throws Exception {
		LOG.debug("element trigger window.max={} watermark.ts={}", window.maxTimestamp(), ctx.getCurrentWatermark()) ;
		if (to.getType() == EventType.LOGOUT)
			return TriggerResult.FIRE_AND_PURGE ;
		else
	        return TriggerResult.CONTINUE ;
	}

	@Override
	public TriggerResult onEventTime(long time, GlobalWindow window, TriggerContext ctx) {
		LOG.debug("event trigger window.max={} ts={}", window.maxTimestamp(), time) ;
		return TriggerResult.CONTINUE ;
	}

	@Override
	public TriggerResult onProcessingTime(long time, GlobalWindow window, TriggerContext ctx) throws Exception {
		LOG.debug("processing trigger window.max={} ts={}", window.maxTimestamp(), time) ;
		return TriggerResult.CONTINUE ;
	}

	@Override
	public void clear(GlobalWindow window, TriggerContext ctx) throws Exception {
	}

	@Override
	public boolean canMerge() {
		return false ;
	}

	@Override
	public void onMerge(GlobalWindow window, OnMergeContext ctx) {}

	@Override
	public String toString() {
		return "SessionMessageToTrigger()" ;
	}

	public static SessionMessageToTrigger create() {
		return new SessionMessageToTrigger() ;
	}
}
