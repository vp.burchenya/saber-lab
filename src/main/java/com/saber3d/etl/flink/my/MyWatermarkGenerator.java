package com.saber3d.etl.flink.my ;

import com.saber3d.etl.flink.model.SessionMessageTo ;

import org.apache.flink.api.common.eventtime.Watermark ;
import org.apache.flink.api.common.eventtime.WatermarkGenerator ;
import org.apache.flink.api.common.eventtime.WatermarkOutput ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;


public class MyWatermarkGenerator implements WatermarkGenerator<SessionMessageTo> {
    private static final Logger LOG = LoggerFactory.getLogger(MyWatermarkGenerator.class) ;
    private long maxTimestamp = Long.MIN_VALUE ;

    @Override
    public void onEvent(SessionMessageTo ev, long eventTimestamp, WatermarkOutput out) {
        LOG.debug("ev.ts={} ev.body={}", eventTimestamp, ev) ;
        maxTimestamp = Math.max(eventTimestamp, maxTimestamp) ;
    }

    @Override
    public void onPeriodicEmit(WatermarkOutput out) {
        LOG.debug("emit watermark.ts={}", maxTimestamp) ;
        out.emitWatermark(new Watermark(maxTimestamp)) ;
    }
}
