package com.saber3d.etl.flink.my ;

import com.saber3d.etl.flink.kafka.BaseKafkaConsumer;


public class PingToKafkaConsumer extends BaseKafkaConsumer<PingTo> {
    public PingToKafkaConsumer(String topic) {
        super(topic, new PingToDeserializationSchema()) ;
    }
}
