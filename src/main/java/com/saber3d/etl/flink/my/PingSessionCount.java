package com.saber3d.etl.flink.my ;

import org.apache.flink.api.common.restartstrategy.RestartStrategies ;
import org.apache.flink.streaming.api.CheckpointingMode ;
import org.apache.flink.streaming.api.TimeCharacteristic ;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator ;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment ;
import org.apache.flink.streaming.api.functions.ProcessFunction ;
import org.apache.flink.streaming.api.functions.sink.SinkFunction ;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction ;
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows ;
import org.apache.flink.streaming.api.windowing.time.Time ;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow ;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer ;
import org.apache.flink.util.Collector ;

import org.apache.flink.util.OutputTag ;
import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;

import java.time.Instant ;
import java.util.UUID ;


public class PingSessionCount {
    private static final Logger LOG = LoggerFactory.getLogger(PingSessionCount.class) ;

    @SuppressWarnings("RedundantThrows")
    public static void main(String[] args) throws Exception {
        LOG.info("start job") ;
        final OutputTag<PingTo> errorTag = new OutputTag<>("error") {} ;
        final StreamExecutionEnvironment env = getEnv() ;
        final FlinkKafkaConsumer<PingTo> consumer = new PingToKafkaConsumer("com.saber3d.ping") ;

        SingleOutputStreamOperator<PingTo> ds =
        env
        .addSource(consumer)
        .setParallelism(1)  // Be careful with kafka partitions, parallelism and watermarks assigner
        .assignTimestampsAndWatermarks(new MyWatermarkAssigner())
        /*
        .assignTimestampsAndWatermarks(WatermarkStrategy
            .<PingTo>forBoundedOutOfOrderness(Duration.ofSeconds(10))
            .withTimestampAssigner((to, recordTs) -> to.getDateTime().toInstant().toEpochMilli())
            //.withIdleness(Duration.ofSeconds(10))
        )
         */
        .process(new ProcessFunction<>() {
            @Override
            public void processElement(PingTo to, Context ctx, Collector<PingTo> out) throws Exception {
                LOG.debug(
                    "income ping.id={} time='{}' processing.ts='{}' watermark.ts='{}'",
                    to.getId(), to.getDateTime().toInstant(),
                    Instant.ofEpochMilli(ctx.timestamp()),
                    Instant.ofEpochMilli(ctx.timerService().currentWatermark())
                ) ;
                out.collect(to) ;
            }
        })
        //.name("to")
        ;

        ds
        .keyBy(PingTo::getId)
        .window(EventTimeSessionWindows.withGap(Time.seconds(10)))  // Use the GAP for periodic per minute calculate (logic in trigger)
        // .allowedLateness(Time.seconds(3))  // Late merging fire
        .trigger(MyPingToEventTimeTrigger.create())
        .process(new ProcessWindowFunction<PingTo, PingTo, UUID, TimeWindow>() {
            @Override
            public void process(UUID session, Context ctx, Iterable<PingTo> elements, Collector<PingTo> out) throws Exception {
                LOG.debug(
                    "window time={} processing.ts={} watermark.ts={} ",
                    (ctx.window().getEnd() - ctx.window().getStart()) / 1000/ 60,
                    Instant.ofEpochMilli(ctx.currentProcessingTime()),
                    Instant.ofEpochMilli(ctx.currentWatermark())
                ) ;
                elements.forEach((el) -> {
                    System.out.println(el) ;
                    out.collect(el) ;
                }) ;
            }
        })
        .addSink(new SinkFunction<PingTo>() {
            @Override
            public void invoke(PingTo to, Context ctx) throws Exception {
                System.out.println("collect=" + to) ;
            }
        })
        // .name("output")
        ;

        ds.getSideOutput(errorTag).addSink(new SinkFunction<PingTo>() {
            @Override
            public void invoke(PingTo to, Context ctx) throws Exception {
                System.err.println(to) ;
            }
        }) ;

        LOG.debug(env.getExecutionPlan()) ;
        env.disableOperatorChaining().execute(PingSessionCount.class.getCanonicalName()) ;
    }

    private static StreamExecutionEnvironment getEnv() {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment() ;
        env.setRestartStrategy(RestartStrategies.noRestart()) ;
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime) ;
        env.enableCheckpointing(60*1000 , CheckpointingMode.EXACTLY_ONCE) ;  // Disable consumer autocommit
        env.disableOperatorChaining() ;
        env.getConfig().setAutoWatermarkInterval(1000) ;
        return env ;
    }
}
