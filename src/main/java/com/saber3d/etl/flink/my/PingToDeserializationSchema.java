package com.saber3d.etl.flink.my ;

import com.saber3d.etl.flink.ser.AJsonDeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation ;
import org.apache.kafka.clients.consumer.ConsumerRecord ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;

import java.io.IOException ;
import java.nio.charset.StandardCharsets ;
import java.util.Optional ;
import java.util.UUID ;

import org.jetbrains.annotations.Nullable ;

import static org.apache.flink.api.java.typeutils.TypeExtractor.getForClass ;


public class PingToDeserializationSchema extends AJsonDeserializationSchema<PingTo> {
    private static final long serialVersionUID = 1L ;
    private static final Logger LOG = LoggerFactory.getLogger(PingToDeserializationSchema.class) ;

    @Override
    public @Nullable PingTo deserialize(ConsumerRecord<byte[], byte[]> record) throws IOException {
        LOG.debug(
            "deserialize key={} value={}",
            new String(Optional.ofNullable(record.key()).orElse(new byte[]{}), StandardCharsets.UTF_8),
            new String(Optional.ofNullable(record.value()).orElse(new byte[]{}), StandardCharsets.UTF_8)
        ) ;

        if (record.value() != null) {
            PingTo to = mapper.readValue(record.value(), PingTo.class) ;
            to.setId(Optional.ofNullable(record.key()).map(
                buf -> UUID.fromString(new String(buf))).orElse(to.getId())
            ) ;
            return to ;
        } else
            return null ;
    }

    @Override
    public boolean isEndOfStream(PingTo nextElement) {
        return false ;
    }

    @Override
    public TypeInformation<PingTo> getProducedType() {
        return getForClass(PingTo.class) ;
    }
}
