package com.saber3d.etl.flink.my ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonFormat ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonProperty ;

import java.util.Date ;
import java.util.UUID ;

import org.jetbrains.annotations.Nullable ;


public class PingTo {
    @JsonProperty("id")
    private UUID id;
    @JsonProperty("ts")
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT, timezone = "UTC")
    private Date dateTime ;
    @Nullable
    private Integer code ;

    public UUID getId() { return id;}
    public void setId(UUID id) { this.id = id;}
    public Date getDateTime() { return dateTime ;}
    public @Nullable Integer getCode() { return code ;}
}
