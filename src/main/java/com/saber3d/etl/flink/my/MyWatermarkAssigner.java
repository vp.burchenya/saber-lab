package com.saber3d.etl.flink.my ;

import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks ;
import org.apache.flink.streaming.api.watermark.Watermark ;

import org.jetbrains.annotations.Nullable ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;


public class MyWatermarkAssigner implements AssignerWithPunctuatedWatermarks<PingTo> {
    private static final Logger LOG = LoggerFactory.getLogger(MyWatermarkAssigner.class) ;
    private long maxTimestamp = Long.MIN_VALUE ;

    @Nullable
    @Override
    public Watermark checkAndGetNextWatermark(PingTo ev, long eventTimestamp) {
        LOG.debug("ev.ts={} ev.body={}", eventTimestamp, ev) ;
        maxTimestamp = Math.max(eventTimestamp, maxTimestamp) ;
        LOG.debug("emit watermark.ts={}", maxTimestamp) ;
        return new Watermark(maxTimestamp) ;
    }

    @Override
    public long extractTimestamp(PingTo ev, long recordTimestamp) {
        return ev.getDateTime().toInstant().toEpochMilli() ;
    }
}
