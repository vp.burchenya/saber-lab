package com.saber3d.etl.flink.my ;

import org.apache.flink.annotation.PublicEvolving ;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger ;
import org.apache.flink.streaming.api.windowing.triggers.Trigger ;
import org.apache.flink.streaming.api.windowing.triggers.TriggerResult ;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;

import java.time.Instant ;
import java.util.Optional ;


@PublicEvolving
public class MyPingToEventTimeTrigger extends Trigger<PingTo, TimeWindow> {

	private static final long serialVersionUID = 1L ;
	private static final Logger LOG = LoggerFactory.getLogger(MyPingToEventTimeTrigger.class) ;
	private final EventTimeTrigger delegate ;
	
	private MyPingToEventTimeTrigger() {
		delegate = EventTimeTrigger.create() ;
	}

	@Override
	public TriggerResult onElement(PingTo to, long timestamp, TimeWindow window, TriggerContext ctx) throws Exception {
		LOG.debug("element trigger window.max={} watermark.ts={}", Instant.ofEpochMilli(window.maxTimestamp()), Instant.ofEpochMilli(ctx.getCurrentWatermark())) ;

		if (Optional.ofNullable(to.getCode()).orElse(0) == 0) return TriggerResult.FIRE ;
		return delegate.onElement(to, timestamp, window, ctx) ;
	}

	@Override
	public TriggerResult onEventTime(long time, TimeWindow window, TriggerContext ctx) {
		LOG.debug("event trigger window.max={} ts={}", Instant.ofEpochMilli(window.maxTimestamp()), Instant.ofEpochMilli(time)) ;
		return delegate.onEventTime(time, window, ctx) ;
	}

	@Override
	public TriggerResult onProcessingTime(long time, TimeWindow window, TriggerContext ctx) throws Exception {
		LOG.debug("processing trigger window.max={} ts={}", Instant.ofEpochMilli(window.maxTimestamp()), Instant.ofEpochMilli(time)) ;
		return delegate.onProcessingTime(time, window, ctx) ;
	}

	@Override
	public void clear(TimeWindow window, TriggerContext ctx) throws Exception {
		delegate.clear(window, ctx) ;
	}

	@Override
	public boolean canMerge() {
		return true ;
	}

	@Override
	public void onMerge(TimeWindow window, OnMergeContext ctx) {
		delegate.onMerge(window, ctx) ;
	}

	@Override
	public String toString() {
		return "MyEventTimeTrigger()" ;
	}

	public static MyPingToEventTimeTrigger create() {
		return new MyPingToEventTimeTrigger() ;
	}
}
