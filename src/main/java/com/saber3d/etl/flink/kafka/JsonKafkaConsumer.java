package com.saber3d.etl.flink.kafka ;

import com.saber3d.etl.flink.ser.JsonKeyValueDeserializationSchema ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode ;


public class JsonKafkaConsumer extends BaseKafkaConsumer<ObjectNode> {
    public JsonKafkaConsumer(String topic) {
        super(topic, new JsonKeyValueDeserializationSchema(true)) ;
    }
    public JsonKafkaConsumer(String topic, ObjectMapper mapper) {
        super(topic, new JsonKeyValueDeserializationSchema(true, mapper)) ;
    }

    public static ObjectMapper createDefaultMapper() {
        return JsonKeyValueDeserializationSchema.createDefaultMapper() ;
    }
}
