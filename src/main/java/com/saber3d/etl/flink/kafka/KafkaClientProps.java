package com.saber3d.etl.flink.kafka ;

import org.apache.flink.api.java.utils.ParameterTool ;
import org.apache.kafka.clients.consumer.ConsumerConfig ;
import org.apache.kafka.clients.producer.ProducerConfig ;

import java.util.Properties ;


public class KafkaClientProps {
    static final Properties props = new Properties() ;
    private static final String login = "saber3d" ;
    private static final String group = login ;
    private static final String secret = "s1NVSeN8KKrd" ;

    static {
        ParameterTool opt = ParameterTool.fromSystemProperties() ;
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, opt.get("bootstrap.servers", "kafka.oz.net.ru:30094")) ;
        props.put(ConsumerConfig.GROUP_ID_CONFIG, opt.get("group.id", group)) ;
        // props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getCanonicalName()) ;
        // props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getCanonicalName()) ;
        // With checkpointing is not enabled consumer must commits offsets to zookeeper
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true) ;
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 60_000) ;
        // kafkaProps.put("flink.partition-discovery.interval-millis", 10_000) ;  // isn't a known

        props.put("sasl.mechanism", "SCRAM-SHA-512") ;
        props.put("security.protocol", "SASL_SSL") ;
        props.put("ssl.endpoint.identification.algorithm", "") ;  // The current implementation does not support Subject Alternative Names (SAN)
        props.put("ssl.truststore.location", "./keystore.jks") ;
        props.put("ssl.truststore.password", "thaiBo4r") ;
        props.put(
            "sasl.jaas.config", String.format(
            "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\" ;",
            opt.get("sasl.username", login),
            opt.get("sasl.password", secret)
        )) ;

        // Producer
        props.put(ProducerConfig.ACKS_CONFIG, "all") ;
    }
}
