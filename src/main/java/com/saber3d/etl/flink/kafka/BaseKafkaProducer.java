package com.saber3d.etl.flink.kafka ;

import org.apache.flink.api.common.serialization.SerializationSchema ;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer ;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema ;


public class BaseKafkaProducer<T> extends FlinkKafkaProducer<T> {
    public BaseKafkaProducer(String topic, SerializationSchema<T> serializer) {
        super(topic, serializer, KafkaClientProps.props) ;
    }

    public BaseKafkaProducer(String topic, KafkaSerializationSchema<T> serializer) {
        super(topic, serializer, KafkaClientProps.props, FlinkKafkaProducer.Semantic.AT_LEAST_ONCE) ;
    }
}
