package com.saber3d.etl.flink.kafka ;

import com.saber3d.etl.flink.model.SessionMessageTo ;
import com.saber3d.etl.flink.ser.SessionMessageToDeserializationSchema ;


public class SessionMessageToKafkaConsumer extends BaseKafkaConsumer<SessionMessageTo> {
    public SessionMessageToKafkaConsumer(String topic) {
        super(topic, new SessionMessageToDeserializationSchema()) ;
    }
}
