package com.saber3d.etl.flink.kafka ;

import org.apache.flink.api.common.serialization.SimpleStringSchema ;


public class StringKafkaConsumer extends BaseKafkaConsumer<String> {
    public StringKafkaConsumer(String topic) {
        super(topic, new SimpleStringSchema()) ;
    }
}
