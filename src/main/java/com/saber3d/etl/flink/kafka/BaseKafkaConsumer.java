package com.saber3d.etl.flink.kafka ;

import org.apache.flink.api.common.serialization.DeserializationSchema ;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer ;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema ;


public class BaseKafkaConsumer<T> extends FlinkKafkaConsumer<T> {
    public BaseKafkaConsumer(String topic, DeserializationSchema<T> deserializer) {
        super(topic, deserializer, KafkaClientProps.props) ;
        this.setStartFromGroupOffsets() ;
    }

    public BaseKafkaConsumer(String topic, KafkaDeserializationSchema<T> deserializer) {
        // Collections.singletonList("ping") | Pattern.compile("com\\.example\\..+")
        super(topic, deserializer, KafkaClientProps.props) ;
        // this.setStartFromGroupOffsets() ;
        this.setStartFromLatest() ;
    }
}
