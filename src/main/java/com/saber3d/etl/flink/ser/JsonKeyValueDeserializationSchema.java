package com.saber3d.etl.flink.ser ;

import static org.apache.flink.api.java.typeutils.TypeExtractor.getForClass ;
import org.apache.flink.api.common.typeinfo.TypeInformation ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.DeserializationFeature ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.SerializationFeature ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.json.JsonMapper ;
import org.apache.kafka.clients.consumer.ConsumerRecord ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;

import java.nio.charset.StandardCharsets ;
import java.io.IOException ;
import java.util.Optional ;


/**
 * DELETEME
 * Use this if you want to send incorrect documents to another sink
 * require call to: mapper.treeToValue(root, To.class).
 * And also for metadata bubble up.
 */
public class JsonKeyValueDeserializationSchema extends AJsonDeserializationSchema<ObjectNode> {
    private static final long serialVersionUID = 1L ;
    private static final Logger LOG = LoggerFactory.getLogger(JsonKeyValueDeserializationSchema.class) ;

    private final boolean includeMetadata ;

    public JsonKeyValueDeserializationSchema(boolean includeMetadata) {
        this(includeMetadata, createDefaultMapper()) ;
    }

    public JsonKeyValueDeserializationSchema(boolean includeMetadata, ObjectMapper mapper) {
        super(mapper) ;
        this.includeMetadata = includeMetadata ;
    }

    public static ObjectMapper createDefaultMapper() {
        return JsonMapper.builder()
            // TODO: .addModule(new Jdk8Module()).addModule(new JavaTimeModule())  <-- add dependencies
            .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
            .enable(SerializationFeature.INDENT_OUTPUT)
            .build() ;
    }

    @Override
    public ObjectNode deserialize(ConsumerRecord<byte[], byte[]> record) throws IOException {
        LOG.debug(
            "deserialize key={} value={}",
            new String(Optional.ofNullable(record.key()).orElse(new byte[]{}), StandardCharsets.UTF_8),
            new String(Optional.ofNullable(record.value()).orElse(new byte[]{}), StandardCharsets.UTF_8)
        ) ;

        ObjectNode node = mapper.createObjectNode() ;

        boolean error = false ;
        String errorDetail = null ;

        if (record.value() != null) {
            byte[] val = record.value() ;
            try {
                node.set("ev", mapper.readValue(val, JsonNode.class)) ;
            } catch (IOException e) {
                node.put("ev", new String(val, StandardCharsets.UTF_8)) ;
                error = true ;
                errorDetail = "not a valid json document" ;
            }
        }
        node.put("error", error) ;
        node.put("error_detail", errorDetail) ;

        String key = null ;
        if (record.key() != null) {
            key = new String(record.key(), StandardCharsets.UTF_8);
        }
        node.put("key", key) ;

        if (includeMetadata) {
            node
            .putObject("metadata")
            .put("offset", record.offset())
            .put("topic", record.topic())
            .put("partition", record.partition())
            ;
        }

        return node ;
    }

    @Override
    public boolean isEndOfStream(ObjectNode nextElement) {
        return false ;
    }

    @Override
    public TypeInformation<ObjectNode> getProducedType() {
        return getForClass(ObjectNode.class) ;
    }
}
