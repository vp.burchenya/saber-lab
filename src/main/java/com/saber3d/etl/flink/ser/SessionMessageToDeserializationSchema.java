package com.saber3d.etl.flink.ser ;

import com.saber3d.etl.flink.model.SessionMessageTo ;

import org.apache.flink.api.common.typeinfo.TypeInformation ;
import org.apache.kafka.clients.consumer.ConsumerRecord ;

import org.slf4j.Logger ;
import org.slf4j.LoggerFactory ;

import java.io.IOException ;
import java.nio.charset.StandardCharsets ;
import java.util.Optional ;

import static org.apache.flink.api.java.typeutils.TypeExtractor.getForClass ;

import org.jetbrains.annotations.Nullable ;


public class SessionMessageToDeserializationSchema extends AJsonDeserializationSchema<SessionMessageTo> {
    private static final long serialVersionUID = 1L ;
    private static final Logger LOG = LoggerFactory.getLogger(SessionMessageToDeserializationSchema.class) ;

    @Override
    public @Nullable SessionMessageTo deserialize(ConsumerRecord<byte[], byte[]> record) throws IOException {
        LOG.debug(
            "deserialize key={} value={}",
            new String(Optional.ofNullable(record.key()).orElse(new byte[]{}), StandardCharsets.UTF_8),
            new String(Optional.ofNullable(record.value()).orElse(new byte[]{}), StandardCharsets.UTF_8)
        ) ;

        // We also can use parsed tree result from JsonKeyValueDeserializationSchema
        if (record.value() != null)
            return mapper.readValue(record.value(), SessionMessageTo.class) ;
        else
            return null ;
    }

    @Override
    public boolean isEndOfStream(SessionMessageTo nextElement) {
        return false ;
    }

    @Override
    public TypeInformation<SessionMessageTo> getProducedType() {
        return getForClass(SessionMessageTo.class) ;
    }
}
