package com.saber3d.etl.flink.ser ;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.DeserializationFeature ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.SerializationFeature ;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.json.JsonMapper ;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema ;


public abstract class AJsonDeserializationSchema<T> implements KafkaDeserializationSchema<T> {
    private static final long serialVersionUID = 1L ;
    protected final ObjectMapper mapper ;

    public AJsonDeserializationSchema() {
        this(createDefaultMapper()) ;
    }

    public AJsonDeserializationSchema(ObjectMapper mapper) {
        this.mapper = mapper ;
    }

    public static ObjectMapper createDefaultMapper() {
        return JsonMapper.builder()
            // TODO: .addModule(new Jdk8Module()).addModule(new JavaTimeModule())  <-- add dependencies
            .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
            .enable(SerializationFeature.INDENT_OUTPUT)
            .build() ;
    }
}
