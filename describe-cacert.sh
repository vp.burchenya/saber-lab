#!/bin/bash
set -eu -o pipefail

BASE_DIR=$(realpath $(dirname "$0"))

openssl x509 -inform pem -in "$BASE_DIR/kafka.pem" -text -noout
