Attached dump file (.tar) contains sample BI-notable data (logs) about users playing some multiplayer online game (1st person shooter) for few days.

# User Story

 

User can do the following:

- register a new account in the game;

- start the game client and log in. User logs out automatically when client is closed;

- choose game mode, click PLAY NOW button and start playing a match with other players online. Backend system automatically assigns players who are looking for game to matches (matchmaking/MM)

- automatically return to the main menu when match is finished

 

# Terms

 

* User (or player): technically, an account record in the backend DB. Has unique ID and registration date. Users are also automatically assigned to immutable player group on registration

* Player Group: allows to mark user accounts for A/B testing (assume, that users in different groups have some differences in game clients that can affect user experience. All users can still play with each other, player groups do not isolate them

* Game Client: an application installed on end-user's device (i.e. PC), that player actually starts to play the game

* Backend System: a set of microservices that manages user accounts, authorization, matchmaking and other meta-game aspects. All users connect to the Backend when client is started

* User Session: time when particular user is online (connected to the Backend), from sign in (game client start) to sign out (exit from the game client). Each user session has unique ID

* Dedicated Server (DS): a server that manages actual gameplay in particular Game Session (match). Game clients (up to 8 in single game session) connect to the DS after successful matchmaking, play a match and then DS is returned to the pool of free servers, so Backend can assign it to another game session later

* Game Session: single match where few (1-8) users can play on same server. Backend assigns few users from matchmaking queue + 1 DS to the single game session. Game Session is usually finishes by score or time limit (or other triggers, depending on game mode). Each game session has unique ID

* Play Session: game session from single user point of view. Each user in one game session has own playsession

 

Entity relations:

> User (1:M) User Session (1:M) Play Session (M:1) Game Session

 

# Sample data schema definition

 

Attached sample contains four .json.gz dataset files. Each file is a compressed (deflate) line-separated JSON (not a JSON array): one log entry per line.

 

Each file contains logs for given time frame only (6 days). For example, that you can find some User IDs in in match-results.json.gz, but dont find that users in users-registered.json.gz. That means that such users were already registered before given time frame.

 

schema-details.md file contains detailed schema descriptions for each field in data sets.

 

# Problem to solve

 

- Consider that all events (with same schemas as in sample data) are sent through RabbitMQ (RMQ) ("incoming" queue)

- Each event could have random delay in deliver (up to 5 minutes)

- Your processing module should calculate CCU number on the fly (with minimal reasonable delay) based on incoming events (from RMQ) and

  send the peak CCU metric value with 1-minute grain to Redis (key = yyyy-MM-dd-HH-mm, value = CCU number for given minute)

- Your processing module should be able to scale horizontally (minimal deployment: 2 independent instances)

- Your processing module should be able to send an alert message to the "alerts" RMQ queue based on "CCU drop" trigger (when CCU number decreases significantly in short time)

 

We expect following artifacts:

1. Short high-level description (how processing module works)

2. Source code for the processing module

3. Instructions on how to configure and run the module

 

On our side we will deploy and run your module (in 2+ instances)  using test steam ( RMQ / Redis credentials should be easily configurable)