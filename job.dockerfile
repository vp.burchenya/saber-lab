FROM gradle:6.8.1-jdk11 AS builder

RUN mkdir -p /usr/local/src/
WORKDIR /usr/local/src/
COPY ./src src/
COPY ./build.gradle ./settings.gradle ./
RUN gradle --no-daemon build


FROM flink:1.11.3-scala_2.12-java11


RUN mkdir -p $FLINK_HOME/usrlib
COPY --from=builder /usr/local/src/build/libs/saber-sesscount-job.jar $FLINK_HOME/usrlib
COPY keystore.jks $FLINK_HOME


# CMD [
#   "/bin/bash", "-c",
#   "$$JAVA_HOME/bin/java -classpath $$FLINK_CLASSPATH -Xmx1073741824 -Xms1073741824",
#   "-XX:MaxMetaspaceSize=268435456 -Dlog.file=/opt/flink/log/jobmanager.log -Dlogback.configurationFile=file:/opt/flink/conf/logback.xml",
#   "-Dlog4j.configurationFile=file:/opt/flink/conf/log4j.properties org.apache.flink.kubernetes.entrypoint.KubernetesApplicationClusterEntrypoint"
# ]
