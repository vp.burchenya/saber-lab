# users-registered.json.gz

## Source:
Backend / Account Management System

## Meaning:
A new user accounts registered (starts the game for the first time)

## Sample event:
{
	"UserUid": "1e70ee12-bf70-11e8-bce7-0004ffd21f23",
	"Time": 1537734890275,
	"PlayerGroup": "main_group"
}

## Fields:

- UserUid: unique account identifier, also user to refer an account in other log types
- Time: UNIX timestamp (in ms) when account is created and log is reported
- PlayerGroup: identifier of the A/B player group (see player groups term definition in read-me-first.overview.md)


# user-sessions-signin.json.gz

## Source:
Backend / Authentication System

## Meaning:
User starts the game client and logs in to the game (connects to the backend)

## Sample event:
{
	"UserUid": "b1e0126c-5617-11e8-a57b-0004ffde78ca",
	"Time": 1537905052967,
	"Country": "US",
	"UserSessionUid": "4a403ea6-c0fc-11e8-b8fc-4ccc6af97cf1",
	"PrevSession": "9dc03819-c033-11e8-b8fc-4ccc6af97cf1"
}

## Fields:
- UserUid: unique account identifier (same as in users-registered.json, same for all sessions of that user)
- Time: UNIX timestamp of the moment when users signs in (starts the game and connects to the backend)
- Country: 2-letter country ISO code (at the moment of sign in)
- UserSessionUid: unique user session identifier, generated for user session on sign in. Also used in signout and other user session related logs
- PrevSession: UserSessionUid of another (previous) user session of same user, if known

# user-sessions-signout.json.gz

## Source:
Backend / User Session Management System

## Meaning:
User closes the game client (and client sends signout request to the backend). User Sessions can also be closed automatically if client is crashed and does not call backend for few minutes (timeout-based sign out)

## Sample event:
{
	"UserUid": "6c2d6fe1-6f3c-11e8-87c4-0004ffd21d22",
	"Time": 1537905597934,
	"UserSessionUid": "91d59ead-c0f6-11e8-8184-74d435110831",
	"FinishReason": 347
}

## Fields:
- UserUid: unique account identifier (same as in users-registered.json, same for all sessions of that user)
- Time: UNIX timestamp of the moment when users signs out (closes the game and disconnects from the backend)
- UserSessionUid: unique user session identifier, same as in user-sessions-signin.json
- FinishReason: enum with reason of user session termination

## SignOut.FinishReason enum description
- 347 = normal Sign Out (when client is closed by user)
- 361 = session is closed by timeout (no calls from game client for few minutes)
- 427 = double sign in (user connects to the backend with same credentials / account from another game client)
- other values = various game errors

# match-results.json.gz

## Source:
Dedicated Servers

## Meaning:
Match is finished (for any reason), so all users from that match disconnects from the DS and returns to the main game menu, and game server is returned to the pool of free servers (for new matches)

## Sample event:
{
	"GameSessionUid": "9085a84f-c11c-11e8-bce8-0003ffb6d7a2",
	"Time": 1537919887758,
	"Duration": "00:16:21.4856337",
	"FinishReason": 2,
	"IsTeam": null,
	"ModeId": 273085466,
	"MapId": 1617521766,
	"TeamStats": [
		{
			"Outcome": 2,
			"Score": 36,
			"TeamId": 0
		},
		{
			"Outcome": 1,
			"Score": 43,
			"TeamId": 1
		}
	],
	"UserStats": [
		{
			"UserSessionUid": "64d15a1a-c10d-11e8-aeae-1c6f65d9604d",
			"Outcome": 2,
			"Place": 4,
			"Score": 17,
			"Kills": 17,
			"Deaths": 24,
			"Assists": 7,
			"Character": 1037146052
		},
		{
			"UserSessionUid": "d7673578-c10c-11e8-88fc-3085a993cb0e",
			"Outcome": 2,
			"Place": 3,
			"Score": 19,
			"Kills": 19,
			"Deaths": 20,
			"Assists": 10,
			"Character": 1070772718
		},
		{
			"UserSessionUid": "05a306aa-c117-11e8-80b3-3c18a0402b61",
			"Outcome": 1,
			"Place": 2,
			"Score": 20,
			"Kills": 20,
			"Deaths": 22,
			"Assists": 6,
			"Character": 884949467
		},
		{
			"UserSessionUid": "09af4c64-c117-11e8-bd8e-6805ca5a72d8",
			"Outcome": 1,
			"Place": 1,
			"Score": 23,
			"Kills": 25,
			"Deaths": 15,
			"Assists": 8,
			"Character": 1647100325
		}
	]
}

## Fields:
- GameSessionUid: unique identifier of the game session, same for all users who player this match
- Time: UNIX timestamp of the moment when game session is finished on DS
- Duration: how much time the match (actual gameplay) has taken in timespan (HH:mm:ss) format
- FinishReason: reason to finish match, see details below
- IsTeam: not in use
- ModeId: internal identifier of game mode
- MapId: internal identifier of the map (game level)
- TeamStats: contains overall team stats for team-based modes (i.e. TDM)
+ TeamStats.TeamId: identifier of the team (0 or 1 for two-team modes)
+ TeamStats.Score: cumulative score of given team (i.e. all players score sum)
+ TeamStats.Outcome: win or loss
- UserStats: contains a record for each player in the match
+ UserStats.UserSessionUid: user session identifier of the player. See user-sessions-signin.json for details
+ UserStats.Outcome: win or loss
+ UserStats.Place: individual place of the player in final scoreboard
+ UserStats.Score: individual score of the player
+ UserStats.Kills: number of kills player made before match finish
+ UserStats.Deaths: number of times player was killed before match finish
+ UserStats.Assists: number of times player helped his team mate to kill enemy (for team based modes)
+ UserStats.Character: internal identifier of the character (hero) that player chose

## GameSession.FinishReason enum description
- 1 = score limit
- 2 = time limit
- 3 = all players left
- other values - various errors

## GameSession.Outcome enum description
- 1 = Win
- 2 = Loss
- 3 = Draw